# -*- coding: utf-8 -*-

# Scrapy settings for dio project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'dio'

SPIDER_MODULES = ['dio.spiders']
NEWSPIDER_MODULE = 'dio.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'dio (+http://www.yourdomain.com)'

USER_AGENT = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
COOKIES_DEBUG = True