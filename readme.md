# Scrapping para o Diário Oficial do Espírito Santo (DIO)


## Objetivo
Atualmente o [portal do DIO](https://dio.es.gov.br/portal/visualizacoes/diario_oficial) permite pesquisa pela data da publicação *ou* uma string com a possibilidade de restrição por um período.

Dado que o fabricante do software não disponibiliza dados/informações que fomentem a política da dados abertos ou sequer a arquitetura da aplicação, a intenção com esta atividade é possibilitar uma pesquisa semântica no DIO, com base nas informações constantes nos documentos públicos, tais como: tipo do poder, orgão e tipo de documento, sendo que para este último é esperado a extração semântica de dados conforme o seu tipo.


## As tecnologias envolvidas

As tecnologias utilizadas que são mais relevantes

### DIO
* Debian
* Apache
* Twitter bootstrap
* PHP ([CakePHP](http://cakephp.org/))
* AngularJS
* [iText](http://sourceforge.net/projects/itext/) para geração de alguns dos arquivos PDF
> `Notas` com relação ao iText: é uma biblioteca utilizada em Java e .NET que gera PDFs com base em arquivos xml ou banco de dados.


### Neste projeto
* [Scrapy](http://scrapy.org/)


## Mapeamento de URLS/funções/retornos

* Página Inicial:
    * URL: https://dio.es.gov.br/portal/visualizacoes/diario_oficial
    * GET: https://dio.es.gov.br/apifront/portal/edicoes/edicoes_from_data/:
        * Referer: https://dio.es.gov.br/portal/visualizacoes/diario_oficial
        * sem paramaetros:
            * retorna a ultima em um json em
                * erro: bool
                * msg: ""
                * itens[0]:
                    * id:3149
                    * data:"12/06/2015"
                    * suplemento:0
                    * numero:24020
                    * tipo_edicao_id:1
                    * tipo_edicao_nome:"Diário Oficial"
                    * capa:1
                    * paginas:88
                    * message:""
                    * has_html:true
        * Com a data no formato do EUA (comumente utilizada pelo MySQL) (https://dio.es.gov.br/apifront/portal/edicoes/edicoes_from_data/2015-06-10):
            * retorna o mesmo que sem parâmetros
    * GET: https://dio.es.gov.br/apifront/portal/edicoes/imagem_diario/idDoDiario/1/imagem
        * Retorna a imagem jpg (680x935) base64 da capa do diário
    * GET: https://dio.es.gov.br/apifront/portal/edicoes/ultimas_edicoes/
        * sem parâmetros:
            * retorna as 10 ultimas edicoes em json, sendo que itens tem dez elementos e cada elemento vem sem os elementos * paginas, message, has_html


* Pesquisa:
    * Por frase parcial:
        * URL: https://dio.es.gov.br/busca#/p=1&q=a&di=20100101&df=20150613
    * Por fase exata:
        * URL:  https://dio.es.gov.br/busca#/p=1&q=%22a%22&di=20100101&df=20150613&f=true
        * Requisições especiais:
            * json: https://dio.es.gov.br/busca/busca/buscar/"a"/0/di:2010-01-01
            * json: https://dio.es.gov.br/busca/busca/buscar/"a"/0/di:2010-01-01/df:2015-06-13
            * json: https://dio.es.gov.br/busca/busca/buscar/"a"/0/di:2010-01-01/df:2015-06-13
        * Resumo do retorno requisições:
            * Todas retornam basicamente a mesma coisa. Aparentemente o campo que pode variar é o "took". O Referer é: https://dio.es.gov.br/busca
            * \_shards (objeto):
                * total: número de requisições
                * successful: número de requisições bem sucedidas
                * failed: número de requisições que falharam
            * hits:
                * total: ?
                * hits (array com, no máximo, 10 posições. São 10 posições por página):
                    * posição no array (objeto):
                        * \_index:"ionews_es"
                        * \_type:"diario"
                        * \_id: idDoDiario_idDaPagina (exemplo: "3149_61")
                        * fields (objeto):
                            * (todos são arrays) day, month, year, diario_id, pagina
                        * highlight (objeto):
                            * conteudo (array): três posições de texto que são exibidos na página como um "resumo"
                        * termos (terms):
                * facets.FileYear.terms:
                    * array cujo número de posições é igual ao número de anos retornados
                        * term: ano em número
                        * count: contagem de resultados encontrados


* Visualização HTML: https://dio.es.gov.br/portal/visualizacoes/html/#/p:60/e:3148
    * Parâmetros:
        * e: id da edição
        * p: página da edição (não obrigatório)
    * Chama:
        * https://dio.es.gov.br/js/html/html.js
            * Referer: https://dio.es.gov.br/portal/visualizacoes/html/
            * https://dio.es.gov.br/apifront/portal/edicoes/edicao_disponivel/3149
                * Parâmetro: 3149 que é o numero do diário
                * Retorno: json contendo a propriedade 'erro' que deve ser igual a false
            * https://dio.es.gov.br/portal/visualizacoes/view_html_diario/3149
                * Parâmetro: 3149 que é o numero do diário
        * https://dio.es.gov.br/apifront/portal/edicoes/publicacoes_ver_conteudo/157989
            * Referer: https://dio.es.gov.br/portal/visualizacoes/html/
            * Parâmetro:
            * 157989: id do conteúdo (ato)


## Etapas
O curso de ação arquitetado foi:

* ✓ levantamento de requisitos
* scrap inicial
   1. acessar a home e pesquisar o DIO pela data, obtendo o código do diário
   2. obter a visualização do diário a partir de seu códgo
   3. extrair os dados de modo a reproduzir a árvore que fica na esquerda da página de visualização html, a exemplo do que está em dados/gustavo/saida.json, com a diferença de que o conteúdo (ato) deve estar no arquivo.
* Armazenar estes dados no [Apache Solr](http://lucene.apache.org/solr/)
* Analisar os dados de modo a descobrir como melhor organizá-los (em uma base relacional?)
* Realizar o scrap dos atos e colocar todos os dados (separados) em um banco de dados
* Criar interface web para acesso aos dados
* Integrar os dados à um serviço de dados abertos e/ou disponibilizar uma API ?


## Considerações gerais (13/06/2015):

* O último id de um diário é: 3149
* Ultimo id de conteúdo é: 158311 (#FIXME Incorreto.Na listagem html eles não estão em ordem numérica crescente)
