// Código por @GustavoRPS
// Comentários por @gnumoksha

    // array que conterá a saida em tipos javascript
    var out = [];

    // Processa cada elemento li dentro de #tree
    function processOneLi(node) {

        // o elemento span guarda o nome do nó e sua classe define seu tipo
        var spanNode = node.children("span:first");
        // retVal é um objeto javascript que será transformado em json
        var retVal = {
            "name": spanNode.text().replace('\n', '').trim(),
            "type": spanNode.attr("class")
        };

        // se o nó contiver a classe file significa que dentro dele há um elemento a que conterá a tag identificador
        // que contém o código do conteúdo (ato). Este código ficará na propriedade id do objeto json
        if (retVal.type === "file") {
            var aNode = spanNode.children("a");
            retVal.id = aNode.attr("identificador");
        }

        // Pocura elementos ul com um li dentro e, para cada um deles, executa essa função recursivamente
        node.find("> ul > li").each(function() {
            // hasOwnProperty retorna um boleano indicando se o objeto tem a propriedade passada como parâmetro
            if (!retVal.hasOwnProperty("children")) {
                retVal.children = [];
            }
            retVal.children.push(processOneLi($(this)));
        });

        return retVal;
    }

    $("#tree").children("li").each(function() {
        out.push(processOneLi($(this)));
    });


    console.log("got the following JSON from your HTML:", JSON.stringify(out)); 
